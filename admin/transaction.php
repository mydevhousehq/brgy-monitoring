<?php include('header.php');?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include('topbar.php');?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <?php if(isset($_GET['type'])){
            $type=$_GET['type'];?>
            <!-- Page Heading -->
            <h1 class="h3 mb-4 text-gray-800" style="text-transform:capitalize;">Transaction >> <?php echo $_GET['type']?></h1>
            <div class="row">
                <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="get">
                <input type="hidden" name="type" value="<?php echo $type;?>">
                <select name="name" id="" class="form-control" required>
                      <option value=""></option>
                        <?php 
                            // GET usage
                            $datax = get('tbl_resident');
                            foreach ($datax as $rowx) {
                                ?>
                                       <option value="<?php echo $rowx['id'];?>|<?php echo $rowx['fname'];?> <?php echo $rowx['mname'];?> <?php echo $rowx['lname'];?>">
                                       <?php echo $rowx['fname'];?>  <?php echo $rowx['mname'];?> <?php echo $rowx['lname'];?>
                                        </option> 
                                <?php 
                            }
                        ?>
                    </select>
                    <br>
                    <input type="submit" value="generate" class=" btn btn-success">
            </form>
            </div>
            
            <?php 
            if(isset($_GET['name'])){
                $result = $_GET['name'];
                $result_explode = explode('|', $result);
                //echo $result_explode[1]; echo $result_explode[0];
                if($type=="id"){
                    ?>
<br>

<div class="row">
    <div class="col-md-4">
        
        <div class="card print-area" style="width: 18rem;min-height:500px;" >
        <img class="card-img-top" src="https://placehold.it/200x200" alt="Card image cap" style="padding:20%;">
        <div class="card-body">
            <h5 class="card-title"><?php echo $result_explode[1];?></h5>
            <p class="card-text">BRGY. BarangayNAME <br> Resident Official ID</p>
            
        </div>
        </div>


    </div>
    <div class="col-md-4">
    
        <div class="card print-area" style="width: 18rem;min-height:500px;">
        <!-- <img class="card-img-top" src="https://placehold.it/200x200" alt="Card image cap" style="padding:20%;"> -->
        <div class="card-body">
            <br><br>


            <?php 
                //get where field
                $data = get_where_fieldvalue('tbl_personel','position','captain');
                foreach ($data as $row) {
                    // echo $row['name']."<br />\n";

                    ?>
                    <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                    <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                    <?php 
                }

            ?>
            <br>
            <?php 
                //get where field
                $data = get_where_fieldvalue('tbl_personel','position','secretary');
                foreach ($data as $row) {
                    // echo $row['name']."<br />\n";

                    ?>
                    <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                    <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                    <?php 
                }

            ?>

            <br>
            <?php 
                //get where field
                $data = get_where_fieldvalue('tbl_personel','position','treasurer');
                foreach ($data as $row) {
                    // echo $row['name']."<br />\n";

                    ?>
                    <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                    <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                    <?php 
                }

            ?>
            
            
            
        </div>
        </div>

    </div>
</div>





<br>
<input type="button"  onclick="printInvoice();" class="btn btn-primary btn-lg" value="Print">
                    <?php 
                }elseif($type=="clearance"){
?>
                <div class="row print-area" id="print-area" style="background:#fff; padding:100px 0;">

                    <div class="col-md-12 text-center">
                        <h2>Barangay Certificate</h2>
                    </div>

                    <div class="col-md-12 text-center" style="margin:100px 0;">
                        <h2><?php echo $result_explode[1];?></h2>
                    </div>

                    <div class="col-md-4 text-center">
                    <?php 
                        //get where field
                        $data = get_where_fieldvalue('tbl_personel','position','captain');
                        foreach ($data as $row) {
                            // echo $row['name']."<br />\n";

                            ?>
                            <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                            <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                            <?php 
                        }

                    ?>
                    </div>
                    <div class="col-md-4 text-center" ">
                    <?php 
                        //get where field
                        $data = get_where_fieldvalue('tbl_personel','position','secretary');
                        foreach ($data as $row) {
                            // echo $row['name']."<br />\n";

                            ?>
                            <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                            <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                            <?php 
                        }

                    ?>
                    </div>

                    <div class="col-md-4 text-center">
                    <?php 
                        //get where field
                        $data = get_where_fieldvalue('tbl_personel','position','treasurer');
                        foreach ($data as $row) {
                            // echo $row['name']."<br />\n";

                            ?>
                            <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                            <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                            <?php 
                        }

                    ?>
                    </div>

                    
                    
                </div>

                <div class="row">
                <input type="button"  onclick="printInvoice();" class="btn btn-primary btn-lg" value="Print">
                </div>
<?php 
                }elseif($type=="indigency"){
                    ?>

<div class="row print-area" id="print-area" style="background:#fff; padding:100px 0;">

                    <div class="col-md-12 text-center">
                        <h2> Certificate of Indigency</h2>
                    </div>

                    <div class="col-md-12 text-center" style="margin:100px 0;">
                        <h2><?php echo $result_explode[1];?></h2>
                    </div>

                    <div class="col-md-4 text-center">
                    <?php 
                        //get where field
                        $data = get_where_fieldvalue('tbl_personel','position','captain');
                        foreach ($data as $row) {
                            // echo $row['name']."<br />\n";

                            ?>
                            <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                            <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                            <?php 
                        }

                    ?>
                    </div>
                    <div class="col-md-4 text-center" ">
                    <?php 
                        //get where field
                        $data = get_where_fieldvalue('tbl_personel','position','secretary');
                        foreach ($data as $row) {
                            // echo $row['name']."<br />\n";

                            ?>
                            <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                            <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                            <?php 
                        }

                    ?>
                    </div>

                    <div class="col-md-4 text-center">
                    <?php 
                        //get where field
                        $data = get_where_fieldvalue('tbl_personel','position','treasurer');
                        foreach ($data as $row) {
                            // echo $row['name']."<br />\n";

                            ?>
                            <h5 class="card-title"><strong><?php echo $row['name'];?></strong>   </h5>
                            <p class="card-text" style="text-transform:capitalize">Barangay <?php echo $row['position'];?></p>
                            <?php 
                        }

                    ?>
                    </div>

                    
                    
                </div>

                <div class="row">
                <input type="button"  onclick="printInvoice();" class="btn btn-primary btn-lg" value="Print">
                </div>
                    <?php 
                }else{
                    die();
                }






            }else{die();}
            


           
        
        
        }else{
            die();
        }
        ?>
          



        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<?php include('footer.php');?>