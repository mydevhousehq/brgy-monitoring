<?php include('header.php');?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include('topbar.php');?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
<?php 

if(isset($_GET['id'])){

    if(isset($_GET['process'])){
        if($_GET['process']=="update"){
            $data = get_where_fieldvalue('tbl_resident','id',$_GET['id']);
            foreach ($data as $row) {
                ?>
                <div class="row">
                    <div class="col-md-4">
                    
                    <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Update Resident</h6>
                </div>
                <div class="card-body">
                        <Form method="post" action="process.php">
                        <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                        <input type="hidden" name="return" value="<?php fileclass();?>">
                        <input type="hidden" name="process" value="update_resident">

                        
                        <label for="">Civil Status</label><br>
                        <select name="civil" id="" class="form-control">
                            <option value="Single">Single</option>
                            <option value="Married">Married</option>
                            <option value="Separated">Separated</option>
                            <option value="Divorced">Divorced</option>
                        </select><br>

                        <label for="">Contact Number</label><br>
                        <input type="text" name="contact" class="form-control" required><br>

                        <label for="">House Number</label><br>
                        <input type="text" name="a_no" class="form-control" required><br>

                        <label for="">Street</label><br>
                        <input type="text" name="a_street" class="form-control" required><br>


                        <input type="submit" value="Update" class="btn btn-success">
                        </Form>
                </div>
              </div>

                    
                    </div>
                </div>
            <?php 

            }
        }elseif($_GET['process']=="view"){
            
// GET where id 

            // get where field
            $data = get_where_fieldvalue('tbl_resident','id',$_GET['id']);
            foreach ($data as $row) {
               // echo $row['name']."<br />\n";
               ?>

                    <h1 class="h3 mb-4 text-gray-800" style="text-transform:capitalize;"><?php echo $row['fname'];?> <?php echo $row['mname'];?> <?php echo $row['lname'];?></h1>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="card shadow mb-3">
                            <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                <thead class="thead-dark">
                                   
                                </thead>
                                <tbody>
                                    <tr>
                                    <th scope="row">Civil Status</th>
                                    <td><?php echo $row['civil_status'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Career</th>
                                    <td><?php echo $row['career'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Blood Type</th>
                                    <td><?php echo $row['blood_type'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Contact</th>
                                    <td><?php echo $row['contact'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Birthday</th>
                                    <td><?php echo $row['birthday'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Voters </th>
                                    <td><?php echo $row['voters_id'];?></td>
                                    </tr>
                                    
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card shadow mb-3">
                            <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Address</h6>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                <thead class="thead-dark">
                                   
                                </thead>
                                <tbody>
                                    <tr>
                                    <th scope="row">Street Number</th>
                                    <td><?php echo $row['a_no'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Street Name</th>
                                    <td><?php echo $row['a_street'];?></td>
                                    
                                    </tr>
                                    
                                    
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card shadow mb-3">
                            <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Image</h6>
                            </div>
                            <div class="card-body">
                            <?php 
                                if(empty($row['img'])){
                                    ?>
                                    empty
                                     <img src="https://placehold.it/800x500" class="img img-fluid" alt="">
                                     <?php echo $row['img'];?>
                                    <?php
                                }else{
                                    ?>
                                    <!-- <?php echo $row['img'];?> -->
                                     <img src="<?php echo $img.$row['img'];?>" class="img img-fluid" alt="">
                                    <?php 
                                }
                            ?>
                                <!-- <img src="https://placehold.it/800x500" class="img img-fluid" alt=""> -->
                            </div>
                        </div>
                    </div>




                        


                    </div>

                   
                <?php 
            }
        }else{
            echo "undefined";
        }
    }

}



?>
         

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<?php include('footer.php');?>