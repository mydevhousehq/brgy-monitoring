<?php include('header.php');?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include('topbar.php');?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Official List</h1>

            <div class="row">
          <?php 
// GET usage
$data = get('tbl_personel');
foreach ($data as $row) {
    ?>
            <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary"><?php echo $row['position'];?></h6>
                </div>
                <div class="card-body">
                    <form action="process.php" method="post">
                        Current: <strong><?php echo $row['name'];?> </strong>
                        <br>
                    <input type="hidden" name="position" value="<?php echo $row['position'];?>">
                    <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                    <input type="hidden" name="return" value="<?php fileclass();?>">
                    <input type="hidden" name="process" value="update_personel">
                    
                    <br>
                    <select name="name" id="" class="form-control" >
                      <option value=""></option>
                        <?php 
                            // GET usage
                            $datax = get('tbl_resident');
                            foreach ($datax as $rowx) {
                                ?>
                                       <option value="<?php echo $rowx['id'];?>|<?php echo $rowx['fname'];?> <?php echo $rowx['mname'];?> <?php echo $rowx['lname'];?>">
                                       <?php echo $rowx['fname'];?>  <?php echo $rowx['mname'];?> <?php echo $rowx['lname'];?>
                                        </option> 
                                <?php 
                            }
                        ?>
                    </select>
                    <br>
                    <input type="submit" value="update" class=" btn btn-success">
                    </form>
                </div>
              </div>
            </div>
    <?php 

}
          ?>
</div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<?php include('footer.php');?>