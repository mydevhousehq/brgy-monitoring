<?php include('header.php');?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include('topbar.php');?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Residential Management</h1>


          <div class="row">
              <div class="col-md-6">
                 


<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addresident">
  Add Resident
</button>

<div class="modal fade" id="addresident" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
  <div class="modal-dialog  modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Resident</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form action="process.php" method="post" enctype="multipart/form-data">
      <div class="modal-body">
                        <input type="hidden" name="return" value="<?php fileclass();?>">
                        <input type="hidden" name="process" value="add_residents">
                        <label for="">First Name</label><br>
                        <input type="text" name="fname" class="form-control" required><br>

                        <label for="">Middle Name</label><br>
                        <input type="text" name="mname" class="form-control" required><br>

                        <label for="">Last Name</label><br>
                        <input type="text" name="lname" class="form-control" required><br>

                        <label for="">Career</label><br>
                        <input type="text" name="career" class="form-control" required><br>

                        <label for="">Contact Number</label><br>
                        <input type="text" name="contact" class="form-control" required><br>

                        <label for="">Voters ID</label><br>
                        <input type="text" name="vid" class="form-control" required><br>

                        <label for="">House Number</label><br>
                        <input type="text" name="a_no" class="form-control" required><br>

                        <label for="">Street</label><br>
                        <input type="text" name="a_street" class="form-control" required><br>

                        <label for="">Birthday</label><br>
                        <input type="date" name="bday" class="form-control" required><br>

                        <label for="">Image</label><br>
                        <input type="file" name="img" accept="image/png, image/gif, image/jpeg" class="form-control" required><br>

                        <label for="">Civil Status</label><br>
                        <select name="civil" id="" class="form-control">
                            <option value="Single">Single</option>
                            <option value="Married">Married</option>
                            <option value="Separated">Separated</option>
                            <option value="Divorced">Divorced</option>
                        </select><br>

                        


                        <label for="">Blood Type</label><br>
                        <select name="bloodtype" id="" class="form-control">
                            <option value="O+">O+</option>
                            <option value="O-">O-</option>
                            <option value="A+">A+</option>
                            <option value="A-">A-</option>
                            <option value="B+">B+</option>
                            <option value="B-">B-</option>
                            <option value="AB+">AB+</option>
                            <option value="AB-">AB-</option>
                        </select><br>
                        
                        <!-- <input type="submit" value="submit" class="btn btn-success" style="margin-top:20px"> -->
                      
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">ADD</button>
                    </div>
                    </form>
                    </div>
                </div>
                </div>



              </div>
              <div class="col-md-6">
                  <!-- <p>Laborum esse labore officiis fugiat veniam voluptatem cumque aliquam impedit illo quas. Consequuntur debitis distinctio ut sequi laudantium beatae explicabo corporis aliquid quis totam. Deserunt autem ut quia beatae magni.</p> -->
              </div>
          </div>

          <br>
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Residents list</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Middle Name</th>
                      <th>Last Name</th>
                      <th>Options</th>
                      
                    </tr>
                  </thead>
                  <tfoot>
                  <tr>
                      <th>First Name</th>
                      <th>Middle Name</th>
                      <th>Last Name</th>
                      <th>Options</th>
                      
                    </tr>

                   
                  </tfoot>
                  <tbody>
                  <?php 
// GET usage
$data = get('tbl_resident');
foreach ($data as $row) {
    ?>
                    <tr>
                     <td><?php echo $row['fname'];?></td>
                     <td><?php echo $row['mname'];?></td>
                     <td><?php echo $row['lname'];?></td>
                     <td colspan="10">
                        <a href="residents-view.php?id=<?php echo $row['id'];?>&process=update" class="btn btn-warning">Update Details</a>
                        <a href="residents-view.php?id=<?php echo $row['id'];?>&process=view" class="btn btn-info">view full details</a>
                       
                        <form action="process.php" method="post" style="display: inline;">
                        <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                        <input type="hidden" name="return" value="<?php fileclass();?>">
                        <input type="hidden" name="process" value="delete_resident">
                        <input type="submit" value="Remove"  class="btn btn-danger">
                        </form>
                        
                     </td>
                    </tr>
    <?php 

}


?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>




        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<?php include('footer.php');?>