<?php session_start();include('functions.php');?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Barangay System | Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>
<?php 

if($_SERVER['REQUEST_METHOD']=="POST"){
    $conn = getConnection();
    $u = $_POST['user'];
    $p = $_POST['pass'];//md5($_POST['pass']);
    $str= "select * from tbl_user where user=:u and pass=:p";
    $cm=$conn->prepare($str);
    $cm->bindParam(':u', $u);
    $cm->bindParam(':p', $p);
    $cm->execute();
    $user = $cm->rowcount();
    
    if ($user == 0) {
        ?>
          <script>alert('failed');</script>
          <script>
            // window.location.href = '.';
          </script>
        <?php
       
        
    }else{
        
        $_SESSION['user'] = $u;


        ?>
          <script>alert('success');
          window.location.href = './';
          </script>
        <?php 

       

       

    }

}else{
    // echo "not post req";
}

?>


<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block customlogin"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Operator Login</h1>
                  </div>
                  <form class="user" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                    <div class="form-group">
                      <input type="text" name="user" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="username">
                    </div>
                    <div class="form-group">
                      <input type="password" name="pass" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                    </div>
                    <div class="form-group">
                      
                    </div>
                  

                    <input type="submit" value="login" class="btn btn-primary btn-user btn-block">
                   
                  </form>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

    <style>
.customlogin{
background-image:url(../assets/images/login.jpg);
background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
}

    </style>

</body>

</html>
