<?php include('header.php');?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include('topbar.php');?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Household Section</h1>



        <!-- searchform -->
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="get">
            <div class="row">
                <div class="col-md-12">
                    <h4>SEARCH</h4>
                </div>
            </div>
            <div class="row">
            <div class="form-group col-md-4">
                <!-- <label for="exampleInputEmail1">Email address</label> -->
                <input type="text" name="a_no" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="House Number">
              
            </div>
            <div class="form-group  col-md-4">
                <!-- <label for="exampleInputPassword1">Password</label> -->
                <input type="text" name="a_street" class="form-control" id="exampleInputPassword1" placeholder="Street Name">
            </div>
            <div class="col-md-4"> <button  type="submit" class="btn btn-primary">Search</button> </div>
            </div>
           
        </form>
        <!-- end search form -->


        

            <?php 
                if(isset($_GET['a_street']) && isset($_GET['a_no'])){
                    ?>
                        <div class="row"><div class="col-md-12"><h3>Results</h3></div></div>
                    <?php
                    $no = $_GET['a_no'];
                    $st= $_GET['a_street'];
                    $data = custom_query("select * from tbl_resident where a_no='$no' and a_street like'%$st%'");
                    foreach ($data as $row) {
                        //echo $row['id']."<br />\n";
                        ?>

                    
<div class="row">
						<div class="col-md-12">
						 <h1 class="h3 mb-4 text-gray-800" style="text-transform:capitalize;"><?php echo $row['fname'];?> <?php echo $row['mname'];?> <?php echo $row['lname'];?></h1>
						</div>
					</div>
					
					<div class="row">
                    <div class="col-md-6">
                        <div class="card shadow mb-3">
                            <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                <thead class="thead-dark">
                                   
                                </thead>
                                <tbody>
                                    <tr>
                                    <th scope="row">Civil Status</th>
                                    <td><?php echo $row['civil_status'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Career</th>
                                    <td><?php echo $row['career'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Blood Type</th>
                                    <td><?php echo $row['blood_type'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Contact</th>
                                    <td><?php echo $row['contact'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Birthday</th>
                                    <td><?php echo $row['birthday'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Voters </th>
                                    <td><?php echo $row['voters_id'];?></td>
                                    </tr>
                                    
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="card shadow mb-3">
                            <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Address</h6>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                <thead class="thead-dark">
                                   
                                </thead>
                                <tbody>
                                    <tr>
                                    <th scope="row">Street Number</th>
                                    <td><?php echo $row['a_no'];?></td>
                                    </tr>

                                    <tr>
                                    <th scope="row">Street Name</th>
                                    <td><?php echo $row['a_street'];?></td>
                                    </tr>
                                    
                                    
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card shadow mb-3">
                            <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Image</h6>
                            </div>
                            <div class="card-body">
                              

                            <?php 
                                if(empty($row['img'])){
                                    ?>
                                    empty
                                     <img src="https://placehold.it/800x500" class="img img-fluid" alt="">
                                     <?php echo $row['img'];?>
                                    <?php
                                }else{
                                    ?>
                                    <!-- <?php echo $row['img'];?> -->
                                     <img src="<?php echo $img.$row['img'];?>" class="img img-fluid" alt="">
                                    <?php 
                                }
                            ?>

                            </div>
                        </div>
                    </div>



                        <hr>
                        


                    </div>

                        <?php 
                    }


                }
            ?>

        



        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<?php include('footer.php');?>