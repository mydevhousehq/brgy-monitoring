<?php include('header.php');?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php include('topbar.php');?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>

<?php 
$path = "../assets/images/resident/";
// $path = "../api/uploads/";
if($_SERVER['REQUEST_METHOD']=="POST"){
    //add resident
    if($_POST['process']=='add_residents'){


        $fileName2 = $_FILES['img']['name'];
        $tmpName2 = $_FILES['img']['tmp_name'];
        $fileSize2 = $_FILES['img']['size'];
        $fileType2 = $_FILES['img']['type'];
        $path2 =  $path . $fileName2;
        $mv2 = move_uploaded_file($tmpName2, $path2);

        if($mv2){
          $img = $fileName2;
        }else{
          $img = "";
        }

        //insert function usage
        $array = array(
            'fname'=>$_POST['fname'],
            'mname'=>$_POST['mname'],
            'lname'=>$_POST['lname'],
            'career'=>$_POST['career'],
            'contact'=>$_POST['contact'],
            'voters_id'=>$_POST['vid'],
            'birthday'=>$_POST['bday'],
            'civil_status'=>$_POST['civil'],
            'blood_type'=>$_POST['bloodtype'],
            'a_no'=>$_POST['a_no'],
            'a_street'=>$_POST['a_street'],
            'img'=>$img
        );

        if(insert($array,'tbl_resident')){
            ?>
            <script>alert('Added');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
      <?php 
            

        }else{
            ?>
                  <script>alert('failed');
                  window.location.href = '<?php echo $_POST['return'];?>.php';
                  </script>
            <?php 

        }

    //end add
    }



    if($_POST['process']=='update_personel'){
      //
      $result = $_POST['name'];
            $result_explode = explode('|', $result);
            // echo "id: ". $result_explode[0]."<br />";
            // echo "name: ". $result_explode[1]."<br />";


            $conn = getConnection();
        
            $str= "SELECT * FROM `tbl_personel` where `r_id`=:i ";
            $cm=$conn->prepare($str);
            $cm->bindParam(':i', $result_explode[0]);
            //$cm->bindParam(':p', $_POST['position']);
            $cm->execute();
            $count = $cm->rowcount();
            
            if ($count == 0) {
              $array = array(
                'name'=> $result_explode[1],
                'r_id'=>$result_explode[0]
              );
              if(update($array,$_POST['id'],'tbl_personel')){
                  ?>
                  <script>alert('Official Updated');
                  window.location.href = '<?php echo $_POST['return'];?>.php';
                  </script>
                  <?php
              }else{
                  ?>
                  <script>alert('Error on update');
                  window.location.href = '<?php echo $_POST['return'];?>.php';
                  </script>
                  <?php
              }
              
                
            }else{

              if($result_explode[1]==""){
                $array = array(
                  'name'=> '',
                  'r_id'=>$result_explode[0]
                );
                if(update($array,$_POST['id'],'tbl_personel')){
                    ?>
                    <script>alert('Cleared');
                    window.location.href = '<?php echo $_POST['return'];?>.php';
                    </script>
                    <?php
                }else{
                    ?>
                    <script>alert('Error on update');
                    window.location.href = '<?php echo $_POST['return'];?>.php';
                    </script>
                    <?php
                }
              }else{
                ?>
                <script>alert('already exist');
                window.location.href = '<?php echo $_POST['return'];?>.php';
                </script>
                <?php
                
              }
              


              ?>
              <!-- <script>alert('Already Exist');
              window.location.href = '<?php echo $_POST['return'];?>.php';
              </script> -->
              <?php
            }

      //
    }



    //
        if($_POST['process']=="update_resident"){
          echo "update resident"; 

          $array = array(
            'civil_status'=> $_POST['civil'],
            'contact'=> $_POST['contact'],
            'a_no'=> $_POST['a_no'],
            'a_street'=> $_POST['a_street']
          );
          if(update($array,$_POST['id'],'tbl_resident')){
              ?>
              <script>alert('Updated Updated');
              window.location.href = 'residents.php';
              </script>
              <?php
          }else{
              ?>
              <script>alert('Error on update');
              window.location.href = 'residents.php';
              </script>
              <?php
          }


        }
    //

        ##
        if($_POST['process']=="delete_resident"){
          //delete function usage
          if(delete($_POST['id'],'tbl_resident')){
            ?>
            <script>alert('Removed');
            window.location.href = '<?php echo $_POST['return'];?>.php';
            </script>
            <?php
          }else{
            ?>
              <script>alert('Error');
              window.location.href = '<?php echo $_POST['return'];?>.php';
              </script>
              <?php
          }
          
        }
        ##
//end request method
}

?>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<?php include('footer.php');?>