<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
// session_starter();
?>
<body class="<?php fileclass();?>">




<!-- Page Content -->
<div class="container">
<br>
  <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Baranggay Gallery</h1><br>
  <a href="./" class="btn btn-info">BACK</a>

  <hr class="mt-2 mb-5">

  <div class="row text-center text-lg-left">

    <!-- <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="assets/images/1.jpg" alt="">
          </a>
    </div> -->

    <?php 
$files = glob("assets/images/gallery/*.jpg");
foreach($files as $jpg){
  // echo $jpg, "\n";
  ?>
    <div class="col-lg-3 col-md-4 col-6">
      <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="<?php echo $jpg;?>" alt="">
          </a>
    </div> 

    <?php 

}
    ?>
   
  </div>

</div>
<!-- /.container -->

<style>


</style>
 <?php include($partials.'footer.php');?>   