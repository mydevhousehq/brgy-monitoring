<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
// session_starter();
?>
<body class="<?php fileclass();?>">
<?php include($partials.'nav.php');?>
<link rel="stylesheet" href="assets/css/fullpage.min.css">
<link rel="stylesheet" href="assets/css/examples.css">
<div id="fullpage">
	<div class="section " id="home">
        <div class="intro">
            <h1>Home</h1>
            <p>Our aim to provide transparency and simpliest way to process transactions to our constituents and to others who take interest in our beloved Barangay is further extended in this website
</p>
            <!-- <img src="imgs/fullPage.png" alt="fullPage" /> -->
        </div>  
	</div>
	<div class="section" id="gallery">
		<div class="intro">
			<img src="assets/images/gallery.png" alt="Cool" style="width:500px;"/>
			<h1>Gallery</h1>
			<p>These are the images of the past events that are held in our Barangay.</p>
			<p>You can see more about them <a href="gallery.php" >here</a></p>
		</div>
	</div>
	<div class="section" id="about">
		<div class="intro">
			<h1>About</h1>
			<p>Our Barangay is currently location in Tuguegarao City, Caggay, Zone 3.</p>
		</div>
	</div>
    <div class="section" id="officials">
		<div class="intro">
			<h1>Officials</h1>
			<p>These are the current officals of our Barangay from the last election.</p>
            <p>You can view Officials <a href="officials.php">Here</a> </p>
		</div>
	</div>
</div>


 <?php include($partials.'footer.php');?>