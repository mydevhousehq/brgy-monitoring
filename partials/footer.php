   

<!-- scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/fullpage.js"></script>
<script src="assets/js/app.min.js"></script>
<script>
   var myFullpage = new fullpage('#fullpage', {
        sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE', '#ccddff', '#ccddff'],
        anchors: ['home', 'gallery', 'about', 'officials', 'lastPage'],
        menu: '#menu',

        //equivalent to jQuery `easeOutBack` extracted from http://matthewlein.com/ceaser/
        easingcss3: 'cubic-bezier(0.175, 0.885, 0.320, 1.275)'
    });
</script>
</body>
</html>