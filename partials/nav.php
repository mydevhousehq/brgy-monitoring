<nav class="navbar navbar-expand-lg fixed-top  navbar-dark bg-dark">
    <a class="navbar-brand" href="#home">Barangay Information Management System</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav mr-auto" id="menu">
        <li class="nav-item active">
          <a class="nav-link" href="#home" data-menuanchor="home" >Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#gallery" data-menuanchor="gallery">Gallery</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#about" data-menuanchor="about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#officials" data-menuanchor="officials">Officials</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="./admin" >Admin Portal</a>
        </li>
      </ul>
      <!-- <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
      </form> -->
    </div>
  </nav>